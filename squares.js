const {loader} = require('./loader.js');
const {isSquare} = require('./is-square.js');

function countSquares(points) {
    let numOfSquares = 0;
    for (let i = 0; i < points.length; i++) {
        for (let j = i+1; j < points.length; j++) {
            for (let k = j+1; k < points.length; k++) {
                for (let l = k+1; l < points.length; l++) {
                    if (isSquare([
                            points[i],
                            points[j],
                            points[k],
                            points[l]
                        ])
                    ) {
                        numOfSquares++;
                    }
                        
                }
            }
        }
    }
    return numOfSquares;
}

loader(countSquares);






