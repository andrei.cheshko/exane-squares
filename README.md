**Requirements**  
- [Node v6+](https://nodejs.org/en/)

**Instructions**  
1) `git clone https://andrei.cheshko@gitlab.com/andrei.cheshko/exane-squares.git`  
2) `cd exane-squares`  
3) `node squares.js input/1-square.json` or `node squares-better.js input/1-square.json`

**Comments**  
There are 2 programs: `squares.js` and `squares-better.js`. `squares.js` is the implementation of the answer I gave on the interview with O(n^4) complexity. Since I was told on the interview, that it is possible to solve this problem in O(n^2), I decided to try myself.  
  
`squares-better.js` is my attempt at solving it with better than O(n^4). The idea came to me while implementing first version, I realised that we don't really need to try and check every single permutation of points if they form squares. Instead we can focus on distances between the points. We can take a pair of points (a,b) and compute the distance between them. We can then use hashmap to group all pairs of points with same distance from each other by using distnace as a hash key. Unfortunetly, while implementing this (what looked like O(n^2) in best case and O(n^3) in worst, I ran into problem of counting same square multiple times, when using a different point of that square as a starting reference. Given more time, I am sure I can fix it. :)  
  
There is an `input` directory with 3 small test cases. When loading a program, it expects a json file as an argument with the list of points. For example, if I want to test against a set of points with 0 squares, I would run `node squares.js input/zero.json`.  
  
**Interview Questions**  
During the interview, I was not sure about 2 questions: WebSockets Security and JS Object key lookup speed.  

As I mentioned, WebSocket is a new technology and it doesn't handle authentication for you. Application would have to write an authentication layer on top of it. There are a little bit more detailts on [WebSocket Security](https://devcenter.heroku.com/articles/websocket-security) page.  
Also what I forgot about is that there is a `wss` protocol in addition to `ws`. As HTTPS differs from HTTP that it encrypts data being sent over HTTP protocol, data sent over WebSocket using WSS protocol will also be encrypted.
  
In regards to JS Object keys/properties lookup speed, it looks like the answer is a little bit more complex than I expected. Firsty, it depends on JS Engine implementation, so Google's V8 might implement key dictionary differently from Mozilla's SpiderMonkey. But it looks like, as I expected, generally, key lookup on the object is a constant operation O(1), similar to hashmap performance.  
Its important to consider that when creating an object in JS, it inherits a prototype of type Object, which already contains certain keys (like method `hasOwnProperty`). For that reason and to make Object keys more convinient to iterate, ES6 introduced Map and WeakMap. More about Map vs Object (here)[https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Map]. [This blog post](https://chrisrng.svbtle.com/es6-data-structures) provides a good high level overview of all data structures added in ES6.