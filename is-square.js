// We don't need actual distance, we can use square of the distance
// to not compute root (which might cause issues with precision)
function distanceSqrt(p1, p2) {
    // no need to compute square root, since we 
    return (Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
}

function isSquare(points) {
    // square has 4 vertices
    if (!Array.isArray(points) || points.length != 4) {
        return false;
    }

    let distances = [0,0,0,0];

    // Compute distance from 1st point to all others
    for (let i = 1; i < points.length; i++) {
        distances[i] = distanceSqrt(points[0], points[i]);
    }

    // find 2 sides, which are equal. The 3rd verticle will be on the opposite
    // side of the first point and will form diagonal with it.
    
    // Check that the diagonal squared length is double of the side length
    // and make sure the 2nd diagonal is of the same length.
    if (distances[1] === distances[2] && 2*distances[1] === distances[3]) {
        return distances[3] === distanceSqrt(points[1], points[2]);
    }
    else if (distances[1] === distances[3] && 2*distances[1] === distances[2]) {
       return distances[2] === distanceSqrt(points[1], points[3]);
    }
    else if (distances[2] === distances[3] && 2*distances[2] === distances[1]) {
        return distances[1] === distanceSqrt(points[2], points[3]);
    }
    else {
        return false;
    }
}

module.exports = {
    distanceSqrt,
    isSquare
};