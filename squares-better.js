const {loader} = require('./loader.js');
const {isSquare, distanceSqrt} = require('./is-square.js');

//
// Helper function to help us compare 2 sets of points and
// determine if its same points we are comparing
function isSamePoints(points1, points2) {
    if (points1[0] === points2[0] && points1[1] === points2[1]) {
        return true;
    }
    else if (points1[0] === points2[1] && points1[1] === points2[0]) {
        return true;
    }
    else {
        return false;
    }
}

// This is Work in Progress
// Currently, there are issues with this algorithm that it counts same square multiple times.
// I didn't have time to find solution to this problem, but I feel like its close.

// While working on the first version of the solution (n^4), I noticed that we are mostly interested
// in distances from points and that they follow certain rules: i.e. sides and diagonals are equal.

// We can iterate over the pair of points and hash their distances and build a list 
// such pairs with same distances. This operation is O(n^2)

// Iterate over pair of points again and check if we form a square with any other pair of points
// with same distance. best: O(n^2), worst: O(n^3)
function countSquares(points) {
    let numOfSquares = 0;
    let distances = {};

    // Iterate over pairs of points and hash their distances. Group all pairs with same
    // distance under same hash key.
    for (let i = 0; i < points.length; i++) {
        for (let j = i+1; j < points.length; j++) {
            let d = distanceSqrt(points[i], points[j]);
            if (distances[d]) {
                distances[d].push([points[i], points[j]]);
            } else {
                distances[d] = [[points[i], points[j]]];
            }
        }
    }
    
    // Iterate over pairs of points again, but this time check if we form a square with 
    // other points with same distance (exclude us of course)
    for (let i = 0; i < points.length; i++) {
        for (let j = i+1; j < points.length; j++) {
            let otherPoints = distances[distanceSqrt(points[i], points[j])];
            for (let k = 0; k < otherPoints.length; k++) {
                if (!isSamePoints([points[i], points[j]], otherPoints[k])) {
                    if (isSquare([points[i], points[j], otherPoints[k][0], otherPoints[k][1]])) {
                        numOfSquares++;
                    }
                }
            }
        }
    }

    // This result is currently bugged, because we will count same square multiple times.
    return numOfSquares;
}

loader(countSquares);




