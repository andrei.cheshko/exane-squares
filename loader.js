const fs = require("fs");

function loader(countFunction) {
    if (process.argv.length < 3) {
        throw new Error('Input file not specified.');
    }

    const fileName = process.argv[2];

    const input = JSON.parse(fs.readFileSync(fileName));

    if (!input.points) {
        throw new Error('Input file must have a JSON format and must contain key "points"');
    }

    console.log(countFunction(input.points));
}

module.exports = {
    loader
};